# giddy

This project is meant as a learning experience for myself. The goal of this project is to fully reverse engineer the git source control management system.
Since git is open sourced I could simply fork it and examine the source for myself, however that would not provide me the same benefit that trying to fully
reimplement git exactly as it is would. This project is meant explicitly for learning purposes and is not intended to be an improvement or replacement for git.
This project will start small, initially just trying to get the core functionality of git and version control in general (i.e. being able to create a 
new repository, being able to track changes to files, being able to commit changes, etc.)  The end goal however is create a fully functional clone of git
with all of its intricacies, if successful this should hopefully provide me a good deal of experience with command line utility development, as well as 
version control systems, and hopefully provide me with more experience with software development in general.